package diceroller;

import java.util.List;
import java.util.Random;

public class Dice {
    public static final int DEFAULTSIDES = 6;
    public static final int MAXIMUMSIDES = 32;
    private int sides;
    private boolean weighted;

    public Dice() {
        this.sides = DEFAULTSIDES;
    }
    public Dice(int sides) {
        this.sides = validateDiceSides(sides);
    }

    public int getSides(){return sides;}

    public int throwDice(Random rnd) throws NullPointerException {
        int randomized = rnd.nextInt(sides - 1);
        return randomized + 1;
    }

    private int validateDiceSides(int sides) {
        if (sides == 0) {
            return DEFAULTSIDES;
        }
        if (sides < 0) {
            sides *= -1;
        }
        return sides > MAXIMUMSIDES ? MAXIMUMSIDES : sides;
    }

    public static Dice getDiceFromSet(List<Dice> dices, int diceSides) {
        for (Dice dice : dices) {
            if (dice.sides == diceSides)
                return dice;
        }
        return null;
    }
}
