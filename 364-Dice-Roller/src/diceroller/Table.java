package diceroller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Table {
    private static List<Dice> myDices = new ArrayList<>();

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            args = new String[]{"2d6", "1d20"};
        }
        throwInstructions(args);
    }

    public static void throwInstructions(String[] instructions) {
        for (String instruction : instructions) {
            try {
                evaluateInstruction(instruction);
            } catch (WrongInstructionFormatException e) {
                System.out.println("Wrong Format for Dice Instruction: " + instruction);
            } catch (RuntimeException e) {
                //todo - implement exception dealer
            }
        }
    }

    private static void evaluateInstruction(String instruction) throws WrongInstructionFormatException {
        if(instruction.contains("d")) {
            try {
                String[] commands = instruction.split("d");
                commands[0].replace("d", "");
                commands[1].replace("d", "");
                int diceThrows = Integer.parseInt(commands[0]);
                int diceSides = Integer.parseInt(commands[1]);
                throwing(diceSides, diceThrows);
            } catch (NumberFormatException e) {
                throw new WrongInstructionFormatException();
            }
        } else {
            throw new WrongInstructionFormatException();
        }
    }

    private static void throwing(int diceSides, int quantity) {
        Random rnd = new Random();
        Dice dice = Dice.getDiceFromSet(myDices, diceSides);
        if (dice == null) {
            dice = new Dice(diceSides);
            myDices.add(dice);
        }
        System.out.println("Throwing " + diceSides + "-side Dice " + quantity + " time(s):");
        for (int i = 0; i < quantity; i++) {
            try {
                int roll = dice.throwDice(rnd);
                System.out.println("\tThrow #"+(i+1)+": " + roll);
            } catch (RuntimeException e) {
                //todo - implement exception dealer
            }
        }
    }
}
