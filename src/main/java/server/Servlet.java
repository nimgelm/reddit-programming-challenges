package server;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class Servlet {

    @RequestMapping("/")
    public String index() {
        return "Hello Challenger world!";
    }

}
