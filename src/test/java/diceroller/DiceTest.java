package diceroller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DiceTest {

    @Test
    public void testCreationWithNormalSides() {
        System.out.println("Testing Dice creation with normal sides");
        int sides = 12;
        Dice dice = new Dice(sides);
        Assertions.assertNotNull(dice);
        Assertions.assertEquals(sides, dice.getSides());
    }

    @Test
    public void testCreationWithDefaultSides() {
        System.out.println("Testing Dice creation with default sides");
        Dice dice = new Dice();
        Assertions.assertNotNull(dice);
        Assertions.assertEquals(Dice.DEFAULTSIDES, dice.getSides());
    }

    @Test
    public void testCreationWithNegativeSides() {
        System.out.println("Testing Dice creation with negative sides");
        int sides = -8;
        Dice dice = new Dice(sides);
        Assertions.assertNotNull(dice);
        Assertions.assertEquals(-sides, dice.getSides());
    }


    @Test
    public void testCreationWithZeroSides() {
        System.out.println("Testing Dice creation with zero sides");
        int sides = 0;
        Dice dice = new Dice(sides);
        Assertions.assertNotNull(dice);
        Assertions.assertEquals(Dice.DEFAULTSIDES, dice.getSides());
    }
}
